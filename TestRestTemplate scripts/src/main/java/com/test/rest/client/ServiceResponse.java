package com.test.rest.client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ServiceResponse implements Serializable {

	private List<String> data = new ArrayList<String>();

	private String status;

	public ServiceResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceResponse(List<String> data, String status) {
		super();
		this.data = data;
		this.status = status;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
