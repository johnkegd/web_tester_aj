package com.test.rest.client.handle.conection;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class TokenHandler {
	
	static final String URL_LOGIN = "https://api.puig.tv/es/login";

	static final String URL_PRODUCTS = "https://api.puig.tv/es/products";
	static final String URL_EMPLOYEES_JSON = "http://localhost:8080/employees.json";
	

	public static String Token ;
	public static String Status;

	
	public static void getToken() {
	// SpringApplication.run(TestRestTemplateApplication.class, args);

			// HttpHeaders
			HttpHeaders headers = new HttpHeaders();

			headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
			// Request to return JSON format
			headers.setContentType(MediaType.APPLICATION_JSON);
//			headers.set("username", "DAVID");
//			headers.set("password", "carenadosgp");

			MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<String, String>();
			parametersMap.add("username", "C5268A");
			parametersMap.add("password", "carenadosgp");

			// HttpEntity<String>: To get result as String.
			HttpEntity<String> entity = new HttpEntity<String>(headers);

			RestTemplate restTemplate = new RestTemplate();

			// Send request with GET method and default Headers.
//			ResponseEntity<String> response = restTemplate.exchange(URL_EMPLOYEES, //
//					HttpMethod.GET, entity, String.class);
	//
//			String result = response.getBody();

			String result = restTemplate.postForObject(URL_LOGIN, parametersMap, String.class);

//			System.out.println("token: " + result.getData().get(0));
//			System.out.println("status: " + result.getStatus());

			System.out.println("result: " + result.substring(44));
			System.out.println("Token: " + result.substring(44, 104));

			String token = result.substring(44, 104);
			System.out.println("token: " + token);

			
}
	
	public static String getStatus() {
		return Status;
	}


	public static void setStatus(String status) {
		Status = status;
	}


	public static String getUrlLogin() {
		return URL_LOGIN;
	}


	public static String getUrlProducts() {
		return URL_PRODUCTS;
	}


	public static void setToken(String token) {
		Token = token;
	}

}
